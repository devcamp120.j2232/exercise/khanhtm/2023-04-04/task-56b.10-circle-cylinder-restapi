package com.example.demo.CircleCylinder;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
    @GetMapping("/circle-area")
    public double getCircleArea(){
        Circle circle = new Circle(5.0 , "yellow");

        return circle.getArea();

       
    }

    @GetMapping("/cylinder-volume")
    public double getCylinderVolume(){

        Cylinder cylinderVolume = new Cylinder(5.0 , "yellow" , 32.3);

        return cylinderVolume.getVolume(); 


    }
}
